import api from "../../utils/api"

export const fetchPost = () => async dp => {
    let response = await api.get('tutorials');
    return dp({
        type: "FETCH_POST",
        payload: response.data.data
    })
}
